# Contributor:  danyf90 <daniele.formichelli@gmail.com>
# Contributor: Philipp 'TamCore' B. <philipp [at] tamcore [dot] eu>
# Contributor: Jakub Schmidtke <sjakub-at-gmail-dot-com>
# Contributor: Christoph Brill <egore911-at-gmail-dot-com>
# Contributor: Lubomir 'Kuci' Kucera <kuci24-at-gmail-dot-com>
# Contributor: Tad Fisher <tadfisher at gmail dot com>
# Contributor: Philippe Hürlimann <p@hurlimann.org>
# Contributor: Julian Raufelder <aur@raufelder.com>
# Contributor: Dhina17 <dhinalogu@gmail.com>
# Maintainer: Kordian Bruck <k@bruck.me>

pkgname=android-studio
pkgver=2021.2.1.16
pkgrel=1
pkgdesc="The official Android IDE (Stable branch)"
arch=('x86_64')
url="https://developer.android.com"
license=('APACHE')
options=('!strip')
makedepends=('imagemagick')
source=("https://dl.google.com/dl/android/studio/ide-zips/${pkgver}/android-studio-${pkgver}-linux.tar.gz"
        "license.html::https://aur.archlinux.org/cgit/aur.git/plain/license.html?h=android-studio"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname/-/_}.desktop")
sha256sums=('aa5773a9e1da25bdb2367a8bdd2b623dbe0345170ed231a15b3f40e8888447dc'
            '9a7563f7fb88c9a83df6cee9731660dc73a039ab594747e9e774916275b2e23e'
            'f5a6a9583121b0ba23a18c96f07b9ef28923ec826cc0f4051e7334255822a96d'
            'c51e3332c3d6e4f288a761f13ac0b0e498ac421ac12a590e7850f26ce4ed6acc')

package() {
    depends=('alsa-lib' 'freetype2' 'libxrender' 'libxtst' 'which')
    optdepends=('gtk2: GTK+ look and feel'
                'libgl: emulator support'
                'ncurses5-compat-libs: native debugger support')

    cd ${srcdir}/${pkgname}

    # Install the application
    install -d ${pkgdir}/{opt/${pkgname},usr/bin}
    cp -a bin lib jre plugins license LICENSE.txt build.txt product-info.json ${pkgdir}/opt/${pkgname}
    ln -s /opt/${pkgname}/bin/studio.sh ${pkgdir}/usr/bin/${pkgname}

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/${pkgname}/LICENSE.txt"  "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/license.html" "${pkgdir}/usr/share/licenses/${pkgname}/license.html"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/opt/android-studio/bin/studio.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    chmod -R ugo+rX ${pkgdir}/opt
}
